import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import VueSwal from 'vue-swal';

Vue.use(VueSwal)
Vue.config.productionTip = false
require('./globals/app');
new Vue({
    router,
  render: h => h(App),
}).$mount('#app')
