import Router from 'vue-router';
import Vue from 'vue';
import About from "./components/about.vue";
import Terms from "./components/terms.vue";
import Privacy from "./components/privacy.vue";
import Home from './components/Home.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path : '/',
            name : 'home',
            component : Home
        },
        {
            path : '/about',
            name : 'about',
            component : About
        },

        {
            path : '/privacy-policy',
            name : 'privacy',
            component : Privacy
        },
        {
            path : '/terms-of-use',
            name : 'terms',
            component : Terms
        }
    ]
})
