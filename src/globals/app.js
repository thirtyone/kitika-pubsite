
/**
 * API path and version
 */
window.api_url = process.env.VUE_APP_API_URL+process.env.VUE_APP_API_VERSION;



/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');


/**
 * Links for redirection of nav buttons
 */

const links = {
    LOCAL : 'http://localhost:8080/',
    DEVELOPMENT : 'http://myaccount-dev.kitika.com/',
    STAGING : 'https://myaccount-staging.kitika.com/',
    PRODUCTION : 'https://myaccount.kitika.com/'
}

export {
    links
}

